# Subscription Mapping Populator
Cli to retrieve data from active subscriptions and populate the dynamodb mapping table
# Running
Execute the `cmd/main.go` file. The following flags can be configured

|              Flag              |  Type  |                          Description                          |                     
|:------------------------------:|:------:|:-------------------------------------------------------------:|
|           TableName            | string |                dynamodb table for subs mapping                |                   
|             Region             |  bool  |                        region for aws                         |
|            Endpoint            |  bool  |                endpoint for local development                 |
|      RetrieveAccountsFlag      |  bool  |             flag to retrieve all active accounts              |
|     RetrieveActiveSubsFlag     |  bool  |           flag to retrieve all active subscriptions           |
|      RetrieveActivePlans       | string |               flag to retrieve all active plans               |
| FillCsvFileWithActivePlansFlag | string |      flag to retrieve  active plans and fill csv report       |
|   PopulateDynamoDbTableFlag    | string |   flag to populate dynamodb table for mapping subscriptions   | 
|         TenantRecordId         | string | tenant record id to use for the query on active subscriptions |
|      LastAccountRecordId       | string |         last account record Id that we want to query          | 
|    QueryRecordIdLowerLimit     | string |         first account record id that we want to query         |



|   Flag   |  Type  | Description |                     
|:--------:|:------:|:-----------:|
|   Host   | string |   db Host   |                   
| Username | string | db Username |
| Password | string | db password |
|   Port   |  int   |   db port   |
|  DbName  | string |   db name   |


### Example

#### Retrieve all accounts
this command creates a csv file that contains all the accounts in the db for this specific tenant
```
 go run cmd/main.go  -RetrieveAccountsFlag -TenantRecordId 1 -Host "localhost" -Username "Username" -Password "Password" -Port 3306 -DbName "killbill"
```

#### Retrieve all active subscriptions
this command returns all the active subscriptions in the range of the decided accountRecordId from the query. The last account RecordId retrieved will be concatenated with the name of the file
the csv file will have accountId, accountRecordId, TenantId, SubscriptionId
```
go run cmd/main.go  -RetrieveActiveSubsFlag  -QueryRecordIdLowerLimit 1 -LastAccountRecordId 1000 -Host "localhost" -Username "Username" -Password "Password" -Port 3306 -DbName "killbill"
```

#### Retrieve all active plans
this command will retrieve all active plans in the specified range of account recordId and the results will be mapped to the existing csv file to add the planName column
```
 go run cmd/main.go  -FillCsvFileWithActivePlansFlag -LastAccountRecordId 1000 -Host "localhost" -Username "Username" -Password "Password" -Port 3306 -DbName "killbill"
```
#### Retrieve all active plans
this command will populate the dynamodb table if record in the csv is not already present in its combination of primary key and sort key
```
 go run cmd/main.go  -PopulateDynamoDbTableFlag -TableName "ddb-localstack-subscription-plan-mapping -region "eu-west-2" -LastAccountRecordId 1000 
```

this is the order of the operations as it should be done:
- retrieve all the accounts
- retrieve all the active subscriptions
- retrieve all active plans and fill the csv file
- populate dynamodb table

because the LastAccountRecordId is used to run the queries and to read the right csv file from the folder it is advised to run a full cycle before starting another one
