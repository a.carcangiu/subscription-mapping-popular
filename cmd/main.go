package main

import (
	"log"
	"project/cmd/settings"
	"project/internal/dynamoclient"
	"project/internal/mysql"
	"project/internal/service"
)

func main() {
	cfg, dbCfg := settings.ReadSettings()
	if cfg == (settings.Settings{}) && dbCfg == (settings.DBSettings{}) {
		log.Fatal("could not read the settings to start the application")
	}
	DynamoClient := dynamoclient.NewDynamoDbClient(cfg.Region, cfg.Endpoint)
	activeSubsSvc := dynamoclient.NewPopulateDynamoService(DynamoClient)

	mysqlClient, err := mysql.NewClient(dbCfg, true)
	if cfg.RetrieveAccounts || cfg.RetrieveActiveSubscriptions || cfg.RetrieveActivePlans {
		if err != nil {
			log.Fatalf("could not get mysql db client: %s", err)
		}
	}
	srv := service.NewService(activeSubsSvc, mysqlClient, cfg)
	srv.Start()
}
