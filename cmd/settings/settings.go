package settings

import (
	"flag"
)

type Settings struct {
	TableName                   string
	Region                      string
	Endpoint                    string
	RetrieveAccounts            bool
	RetrieveActiveSubscriptions bool
	RetrieveActivePlans         bool
	FillCsvWithActivePlans      bool
	PopulateDynamoDbTable       bool
	TenantRecordId              string
	TenantId                    string
	LastAccountRecordId         int
	QueryRecordIdUpperLimit     int
	QueryRecordIdLowerLimit     int
}

type DBSettings struct {
	Host     string
	Username string
	Password string
	Port     int
	DBName   string
}

func ReadSettings() (Settings, DBSettings) {

	tableName := flag.String("TableName", "", "table to populate with active subscription")
	region := flag.String("Region", "", "region for dynamodb client")
	endpoint := flag.String("Endpoint", "", "endpoint for local run")
	retrieveAccountsFlag := flag.Bool("RetrieveAccountsFlag", false, "flag to retrieve the accounts record id per tenant")
	retrieveActiveSubsFlag := flag.Bool("RetrieveActiveSubsFlag", false, "flag to retrieve the active subscriptions per account")
	fillCsvFileWithActivePlans := flag.Bool("FillCsvFileWithActivePlansFlag", false, "flag to fill all the active subscriptions in the csv report with the active plans")
	populateDynamodbTableFlag := flag.Bool("PopulateDynamoDbTableFlag", false, "flag to populate the subscription plan mapping table on dynamodb")
	tenantRecordId := flag.String("TenantRecordId", "", "tenant record Id ")
	tenantId := flag.String("TenantId", "", "tenant record Id ")
	lastAccountRecordId := flag.Int("LastAccountRecordId", 0, "recordId to retrieve the right file path for csv")
	queryRecordIdLowerLimit := flag.Int("QueryRecordIdLowerLimit", 0, "first recordId to limit the query on rds")
	host := flag.String("Host", "", "db host")
	username := flag.String("Username", "", "db username")
	password := flag.String("Password", "", "db password")
	port := flag.Int("Port", 0, "db port")
	dbName := flag.String("DbName", "", "db name")

	// FOR LOCAL TEST
	//tableName := flag.String("TableName", "subscription-plan-mapping", "table to populate with active subscription")
	//region := flag.String("Region", "eu-west-2", "region for dynamodb client")
	//endpoint := flag.String("Endpoint", "http://localhost:4566", "endpoint for local run")
	//retrieveAccountsFlag := flag.Bool("RetrieveAccountsFlag", true, "flag to retrieve the accounts record id per tenant")
	//retrieveActiveSubsFlag := flag.Bool("RetrieveActiveSubsFlag", false, "flag to retrieve the active subscriptions per account")
	//fillCsvFileWithActivePlans := flag.Bool("FillCsvFileWithActivePlansFlag", false, "flag to fill all the active subscriptions in the csv report with the active plans")
	//populateDynamodbTableFlag := flag.Bool("PopulateDynamoDbTableFlag", false, "flag to populate the subscription plan mapping table on dynamodb")
	//tenantRecordId := flag.String("TenantRecordId", "1", "tenant record Id ")
	//tenantId := flag.String("TenantId", "107f24b5-2d56-4b4f-8463-23b3fe24ebce", "tenant record Id ")
	//lastAccountRecordId := flag.Int("LastAccountRecordId", 4, "recordId to retrieve the right file path for csv")
	//queryRecordIdLowerLimit := flag.Int("QueryRecordIdLowerLimit", 3, "first recordId to limit the query on rds")
	//host := flag.String("Host", "localhost", "db host")
	//username := flag.String("Username", "root", "db username")
	//password := flag.String("Password", "killbill", "db password")
	//port := flag.Int("Port", 3306, "db port")
	//dbName := flag.String("DbName", "killbill", "db name")

	flag.Parse()

	settings := Settings{
		TableName:                   *tableName,
		Region:                      *region,
		Endpoint:                    *endpoint,
		RetrieveAccounts:            *retrieveAccountsFlag,
		PopulateDynamoDbTable:       *populateDynamodbTableFlag,
		RetrieveActiveSubscriptions: *retrieveActiveSubsFlag,
		FillCsvWithActivePlans:      *fillCsvFileWithActivePlans,
		LastAccountRecordId:         *lastAccountRecordId,
		QueryRecordIdLowerLimit:     *queryRecordIdLowerLimit,
		TenantRecordId:              *tenantRecordId,
		TenantId:                    *tenantId,
	}

	dbSettings := DBSettings{
		Host:     *host,
		Username: *username,
		Password: *password,
		Port:     *port,
		DBName:   *dbName,
	}
	return settings, dbSettings
}
