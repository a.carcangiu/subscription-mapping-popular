package dynamoclient

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"project/internal/mysql"
)

type record struct {
	TenantId       string `dynamodbav:"tenantId"`
	AccountId      string `dynamodbav:"accountId"`
	SubscriptionId string `dynamodbav:"subscriptionId"`
	PlanName       string `dynamodbav:"planName"`
	CreatedAt      string `dynamodbav:"createdAt"`
}

type ActiveSubscriptions struct {
	DynamodbClient dynamodb.Client
}

func NewPopulateDynamoService(dynamoClient dynamodb.Client) ActiveSubscriptions {
	return ActiveSubscriptions{
		DynamodbClient: dynamoClient,
	}
}
func NewDynamoDbClient(region, endpoint string) dynamodb.Client {
	cfg, err := config.LoadDefaultConfig(context.Background(),
		config.WithRegion(region),
	)
	if err != nil {
		panic(err)
	}

	client := *dynamodb.NewFromConfig(cfg, func(o *dynamodb.Options) {
		if endpoint != "" {
			o.BaseEndpoint = aws.String(endpoint)
		}
	})
	return client
}

func (a ActiveSubscriptions) GetItem(sub mysql.ActiveSubscriptionsDao, tableName string) (*dynamodb.QueryOutput, error) {
	attributeName := "subscriptionId"
	res, err := a.DynamodbClient.Query(context.Background(), &dynamodb.QueryInput{
		TableName:              aws.String(tableName),
		KeyConditionExpression: aws.String("#subscriptionId = :value"),
		ExpressionAttributeNames: map[string]string{
			"#subscriptionId": attributeName,
		},
		ExpressionAttributeValues: map[string]types.AttributeValue{
			":value": &types.AttributeValueMemberS{Value: sub.SubscriptionId},
		},
	})
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a ActiveSubscriptions) PopulateDynatable(sub mysql.ActiveSubscriptionsDao, tableName string) error {
	record := record{
		TenantId:       sub.TenantId,
		AccountId:      sub.AccountId,
		SubscriptionId: sub.SubscriptionId,
		PlanName:       sub.PlanName,
	}
	input, err := attributevalue.MarshalMap(record)
	if err != nil {
		fmt.Println(err)
	}

	_, err = a.DynamodbClient.PutItem(context.Background(), &dynamodb.PutItemInput{
		TableName: aws.String(tableName),
		Item:      input,
	})
	if err != nil {
		return err
	}
	return err
}
