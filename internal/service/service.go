package service

import (
	"fmt"
	"log"
	"project/cmd/settings"
	csvService "project/internal/csv"
	"project/internal/dynamoclient"
	"project/internal/mysql"
)

type Service struct {
	RetrieveAccounts       bool
	RetrieveActiveSubs     bool
	FillCsvWithActivePlans bool
	PopulateDynamoDbTable  bool
	DynamoDbClient         dynamoclient.ActiveSubscriptions
	TableName              string
	MysqlClient            mysql.Client

	TenantRecordId               string
	TenantId                     string
	BatchSize                    int
	CsvPathPopulateDynamoDBTable string
	LastAccountRecordId          int
	QueryRecordIdLowerLimit      int
}

func NewService(dynamoClient dynamoclient.ActiveSubscriptions, mysqlClient mysql.Client, settings settings.Settings) *Service {
	return &Service{
		DynamoDbClient:          dynamoClient,
		MysqlClient:             mysqlClient,
		RetrieveAccounts:        settings.RetrieveAccounts,
		RetrieveActiveSubs:      settings.RetrieveActiveSubscriptions,
		FillCsvWithActivePlans:  settings.FillCsvWithActivePlans,
		PopulateDynamoDbTable:   settings.PopulateDynamoDbTable,
		TenantRecordId:          settings.TenantRecordId,
		TenantId:                settings.TenantId,
		TableName:               settings.TableName,
		LastAccountRecordId:     settings.LastAccountRecordId,
		QueryRecordIdLowerLimit: settings.QueryRecordIdLowerLimit,
	}
}

func (s *Service) Start() {
	if s.RetrieveAccounts {
		accountRecordIds, err := s.MysqlClient.GetAccounts(s.TenantRecordId)
		if err != nil {
			fmt.Println("GetAccounts error.", err)
			return
		}
		csvService.SaveAccountRecordsAsCSV(accountRecordIds)
	}
	if s.RetrieveActiveSubs {
		activeSubscriptions, err := s.MysqlClient.GetActiveSubscriptions(s.QueryRecordIdLowerLimit, s.LastAccountRecordId)
		if err != nil {
			fmt.Println("GetActiveSubscriptions error.", err)
			return
		}
		accounts, err := csvService.ReadAccounts()
		// The lastAccountRecordId must be the s.LastAccountRecordId for consistency
		//lastAccountRecordId, _ := strconv.Atoi(activeSubscriptions[len(activeSubscriptions)-1].AccountRecordId)
		activeSubsWithAccountId := FillActiveSubsWithAccountId(activeSubscriptions, accounts)
		csvService.SaveResultsAsCSV(activeSubsWithAccountId, s.LastAccountRecordId, s.TenantId)

	}
	if s.FillCsvWithActivePlans {
		activePlans, err := s.MysqlClient.GetAllPlanNamesForActiveSubscriptions(s.QueryRecordIdLowerLimit, s.LastAccountRecordId)
		if err != nil {
			fmt.Println("GetAllPlanNamesForActiveSubscriptions error.", err)
			return
		}
		activeSubsWithAccountId, err := csvService.ReadActiveSubscriptionsWithoutPlanName(s.LastAccountRecordId)
		if err != nil {
			fmt.Println("ReadActiveSubscriptionsWithoutPlanName error.", err)
			return
		}
		activeSubsWithAccountIdAndPlanName := FillPlanNameToActiveSubscriptions(activeSubsWithAccountId, activePlans)
		csvService.SaveResultsAsCSV(activeSubsWithAccountIdAndPlanName, s.LastAccountRecordId, s.TenantId)
	}

	//TODO ADD CHECK FOR EXISTING
	if s.PopulateDynamoDbTable {
		log.Println("starting service to populate dynamodb table")
		activeSubscriptions, err := csvService.ReadActiveSubscriptions(s.LastAccountRecordId)
		if err != nil {
			log.Println("ReadActiveSubscriptions error.", err)
			return
		}
		log.Println("finished reading active subscriptions from csv file")

		for _, sub := range activeSubscriptions {
			items, err := s.DynamoDbClient.GetItem(sub, s.TableName)
			if err != nil {
				log.Printf("error retrieveing record: %s| subscriptionId: %s | accountId: %s | planName: %s", err, sub.SubscriptionId, sub.AccountId, sub.PlanName)
				continue
			}
			if len(items.Items) == 0 {
				err := s.DynamoDbClient.PopulateDynatable(sub, s.TableName)
				if err != nil {
					log.Printf("could not put subscription in dynamodb table: %s, error: %s", sub.SubscriptionId, err)
					continue
				}
				log.Printf("added to table item | subscriptionId: %s | accountId: %s | planName: %s", sub.SubscriptionId, sub.AccountId, sub.PlanName)
			} else {
				log.Printf("item was already present in dynamodb table| subscriptionId: %s | accountId: %s | planName: %s", sub.SubscriptionId, sub.AccountId, sub.PlanName)
			}

		}
	}
}

func FillPlanNameToActiveSubscriptions(subsWithoutPlanName []mysql.ActiveSubscriptionsDao, planNameWithoutSubsIds []mysql.ActiveSubscriptionsDao) []mysql.ActiveSubscriptionsDao {
	var subsWithPlanName []mysql.ActiveSubscriptionsDao
	for _, sub := range subsWithoutPlanName {
		for _, subPlan := range planNameWithoutSubsIds {
			if sub.SubscriptionId == subPlan.SubscriptionId {
				sub.PlanName = subPlan.PlanName
				subsWithPlanName = append(subsWithPlanName, sub)
			}
		}
	}
	return subsWithPlanName
}

func FillActiveSubsWithAccountId(activeSubs []mysql.ActiveSubscriptionsDao, accountIds []mysql.AccountRecords) []mysql.ActiveSubscriptionsDao {
	var listOfActiveSubs []mysql.ActiveSubscriptionsDao
	for _, sub := range activeSubs {
		for _, accountId := range accountIds {
			if sub.AccountRecordId == accountId.AccountRecordId {
				sub.AccountId = accountId.AccountId
				listOfActiveSubs = append(listOfActiveSubs, sub)
			}
		}
	}
	return listOfActiveSubs
}
