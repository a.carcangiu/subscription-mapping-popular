package mysql

import (
	"context"
	"contrib.go.opencensus.io/integrations/ocsql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"project/cmd/settings"

	"github.com/jmoiron/sqlx"
)

type ActiveSubscriptionsDao struct {
	TenantId        string `json:"tenant_id"`
	AccountId       string `json:"account_id"`
	SubscriptionId  string `json:"subscription_id"`
	PlanName        string `json:"plan_name"`
	AccountRecordId string `json:"account_record_id"`
	CreatedAt       string `json:"created_at"`
}

type AccountRecords struct {
	AccountId       string `json:"account_id"`
	AccountRecordId string `json:"account_record_id"`
}

type client struct {
	db *sqlx.DB
}

type Client interface {
	GetActiveSubscriptions(recordIdLowerLimit, recordIdUpperLimit int) ([]ActiveSubscriptionsDao, error)
	GetAccounts(tenantRecordId string) ([]AccountRecords, error)
	GetAllPlanNamesForActiveSubscriptions(recordIdLowerLimit, recordIdUpperLimit int) ([]ActiveSubscriptionsDao, error)
}

func NewClient(cfg settings.DBSettings, tlsEnforcing bool) (Client, error) {

	const dbType = "mysql"

	dbEndpoint := fmt.Sprintf("%s:%d", cfg.Host, cfg.Port)

	driverName, err := ocsql.Register(dbType, ocsql.WithAllTraceOptions())
	if err != nil {
		return nil, err
	}

	tls := ""
	if tlsEnforcing {
		tls = "&tls=true"
	}
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?allowCleartextPasswords=true&parseTime=true%s",
		cfg.Username, cfg.Password, dbEndpoint, cfg.DBName, tls,
	)

	db, err := sqlx.Open(driverName, dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxIdleConns(0)
	if err = db.PingContext(context.Background()); err != nil {
		return nil, err
	}

	return &client{db: db}, nil
}

func (c *client) GetActiveSubscriptions(recordIdLowerLimit, recordIdUpperLimit int) ([]ActiveSubscriptionsDao, error) {
	var activeSubscriptions []ActiveSubscriptionsDao
	//10k first
	queryStatement := `SELECT s.id as subscription_id, s.account_record_id, s.created_date FROM (select * from subscriptions s WHERE s.account_record_id BETWEEN ? AND ?) s LEFT JOIN subscription_events se ON se.subscription_id = s.id AND se.user_type = 'CANCEL' AND se.is_active WHERE se.id IS NULL`
	qResp, err := c.db.Query(queryStatement, recordIdLowerLimit, recordIdUpperLimit)
	if err != nil {
		return nil, err
	}
	for qResp.Next() {
		activeSub := ActiveSubscriptionsDao{}
		err := qResp.Scan(&activeSub.SubscriptionId, &activeSub.AccountRecordId, &activeSub.CreatedAt)
		if err != nil {
			fmt.Println("GetActiveSubscriptions - could not scan results of the query ")
		}
		activeSubscriptions = append(activeSubscriptions, activeSub)
	}

	return activeSubscriptions, nil
}

func (c *client) GetAllPlanNamesForActiveSubscriptions(recordIdLowerLimit, recordIdUpperLimit int) ([]ActiveSubscriptionsDao, error) {
	queryStatement2 := `SELECT DISTINCT ii.subscription_id, ii.plan_name, ii.account_record_id from (select * from invoice_items ii where ii.account_record_id BETWEEN ? AND ?) ii WHERE ii.plan_name IS NOT NULL`
	qResp2, err := c.db.Query(queryStatement2, recordIdLowerLimit, recordIdUpperLimit)
	if err != nil {
		return nil, err
	}

	var activePlanNames []ActiveSubscriptionsDao

	for qResp2.Next() {
		activeSub := ActiveSubscriptionsDao{}
		err := qResp2.Scan(&activeSub.SubscriptionId, &activeSub.PlanName, &activeSub.AccountRecordId)
		if err != nil {
			fmt.Println("GetAllPlanNamesForActiveSubscriptions - could not scan results of the query ")
		}
		activePlanNames = append(activePlanNames, activeSub)
	}
	return activePlanNames, nil
}

func (c *client) GetAccounts(tenantRecordId string) ([]AccountRecords, error) {
	var recordIds []AccountRecords

	queryStatement1 := `SELECT a.id, a.record_id FROM accounts a WHERE a.tenant_record_id = ? and a.created_date < ? ORDER BY a.record_id`
	qResp, err := c.db.Query(queryStatement1, tenantRecordId, "2023-12-06")

	if err != nil {
		return nil, err
	}

	for qResp.Next() {
		recordId := AccountRecords{}
		err = qResp.Scan(&recordId.AccountId, &recordId.AccountRecordId)
		if err != nil {
			fmt.Println("GetAccounts - could not scan results of the query ")
		}
		recordIds = append(recordIds, recordId)
	}

	return recordIds, nil
}
