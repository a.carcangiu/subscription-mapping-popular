package readFile

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"project/internal/mysql"
	"runtime"
	"strconv"
	"strings"
)

const pathAccountIds = "/accounts.csv"
const pathActiveSubscriptions = "/activeSubs"
const pathActiveSubscriptionsWithoutPlanName = "/activeSubs"

func OpenCSVFileAndNewCSVReader(fileName string) (*os.File, *csv.Reader) {
	_, b, _, _ := runtime.Caller(0)
	csvPath := filepath.Dir(b)
	csvFile, err := os.Open(csvPath + fileName)
	if err != nil {
		log.Fatal(err)
	}

	csvReader := csv.NewReader(csvFile)

	return csvFile, csvReader
}

func ReadActiveSubscriptionsWithoutPlanName(lastAccountRecordId int) ([]mysql.ActiveSubscriptionsDao, error) {
	log.Printf("Reading Active Subscriptions from CSV...")

	_, csvReader := OpenCSVFileAndNewCSVReader(pathActiveSubscriptionsWithoutPlanName + strconv.Itoa(lastAccountRecordId) + ".csv")

	var activeSubscriptions []mysql.ActiveSubscriptionsDao

	lineCount := 0
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		// avoiding headers
		if lineCount > 0 {
			activeSub := mysql.ActiveSubscriptionsDao{
				TenantId:        strings.TrimSpace(record[0]),
				AccountId:       strings.TrimSpace(record[1]),
				AccountRecordId: strings.TrimSpace(record[2]),
				SubscriptionId:  strings.TrimSpace(record[3]),
				PlanName:        strings.TrimSpace(record[4]),
				CreatedAt:       strings.TrimSpace(record[5]),
			}
			activeSubscriptions = append(activeSubscriptions, activeSub)
		}
		lineCount++
	}
	return activeSubscriptions, nil
}

func ReadActiveSubscriptions(lastAccountRecordId int) ([]mysql.ActiveSubscriptionsDao, error) {
	log.Printf("Reading Active Subscriptions from CSV...")

	_, csvReader := OpenCSVFileAndNewCSVReader(pathActiveSubscriptions + strconv.Itoa(lastAccountRecordId) + ".csv")

	var activeSubscriptions []mysql.ActiveSubscriptionsDao

	lineCount := 0
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		// avoiding headers
		if lineCount > 0 {
			activeSub := mysql.ActiveSubscriptionsDao{
				TenantId:        strings.TrimSpace(record[0]),
				AccountId:       strings.TrimSpace(record[1]),
				AccountRecordId: strings.TrimSpace(record[2]),
				SubscriptionId:  strings.TrimSpace(record[3]),
				PlanName:        strings.TrimSpace(record[4]),
				CreatedAt:       strings.TrimSpace(record[5]),
			}
			activeSubscriptions = append(activeSubscriptions, activeSub)
		}
		lineCount++
	}
	return activeSubscriptions, nil
}

func ReadAccounts() ([]mysql.AccountRecords, error) {
	log.Printf("Reading Accounts from CSV...")
	_, csvReader := OpenCSVFileAndNewCSVReader(pathAccountIds)

	var accountRecordIds []mysql.AccountRecords

	lineCount := 0
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		// avoiding headers
		if lineCount > 0 {
			account := mysql.AccountRecords{
				AccountId:       strings.TrimSpace(record[0]),
				AccountRecordId: strings.TrimSpace(record[1]),
			}
			accountRecordIds = append(accountRecordIds, account)
		}

		lineCount++
	}
	return accountRecordIds, nil
}

func SaveResultsAsCSV(content []mysql.ActiveSubscriptionsDao, placeholderSubNumber int, tenantId string) {
	_, b, _, _ := runtime.Caller(0)
	csvPath := filepath.Dir(b)
	csvFile, err := os.Create(csvPath + "/activeSubs" + strconv.Itoa(placeholderSubNumber) + ".csv")
	defer func(csvFile *os.File) {
		err := csvFile.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(csvFile)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	w := csv.NewWriter(csvFile)
	defer w.Flush()

	header := []string{"TenantId", "AccountId", "AccountRecordId", "SubscriptionId", "PlanName", "CreatedAt"}
	var data [][]string
	err = w.Write(header)
	if err != nil {
		log.Printf("error writing header for csv file: %s", err)
	}
	for _, record := range content {
		record.TenantId = tenantId
		row := []string{record.TenantId, record.AccountId, record.AccountRecordId, record.SubscriptionId, record.PlanName, record.CreatedAt}
		data = append(data, row)
	}

	err = w.WriteAll(data)
	if err != nil {
		log.Printf("error writing rows on new csv file: %s", err)
	}
}

func SaveAccountRecordsAsCSV(content []mysql.AccountRecords) {
	_, b, _, _ := runtime.Caller(0)
	csvPath := filepath.Dir(b)
	csvFile, err := os.Create(csvPath + pathAccountIds)
	defer func(csvFile *os.File) {
		err := csvFile.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(csvFile)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	w := csv.NewWriter(csvFile)
	defer w.Flush()

	header := []string{"AccountId", "AccountRecordId"}
	var data [][]string
	err = w.Write(header)
	if err != nil {
		log.Printf("error writing header for csv file: %s", err)
	}
	for _, record := range content {
		row := []string{record.AccountId, record.AccountRecordId}
		data = append(data, row)
	}

	err = w.WriteAll(data)
	if err != nil {
		log.Printf("error writing rows on new csv file: %s", err)
	}
}

//func SaveAllActivePlans(content []mysql.ActiveSubscriptionsDao) {
//	_, b, _, _ := runtime.Caller(0)
//	csvPath := filepath.Dir(b)
//	csvFile, err := os.Create(csvPath + "/accounts.csv")
//	defer func(csvFile *os.File) {
//		err := csvFile.Close()
//		if err != nil {
//			fmt.Println(err)
//		}
//	}(csvFile)
//
//	if err != nil {
//		log.Fatalf("failed creating file: %s", err)
//	}
//
//	w := csv.NewWriter(csvFile)
//	defer w.Flush()
//
//	header := []string{"AccountRecordId", "SubscriptionId", "PlanName"}
//	var data [][]string
//	err = w.Write(header)
//	if err != nil {
//		log.Printf("error writing header for csv file: %s", err)
//	}
//	for _, record := range content {
//		row := []string{record.AccountRecordId, record.SubscriptionId, record.PlanName}
//		data = append(data, row)
//	}
//
//	err = w.WriteAll(data)
//	if err != nil {
//		log.Printf("error writing rows on new csv file: %s", err)
//	}
//}
